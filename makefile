PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./pecm.sh $(BINDIR)/pecm
	cp ./pecm.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/pecm
	rm $(MANDIR)/pecm.1

