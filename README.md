![](logo.png)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

### Motivation
I use the [ctmg](https://git.zx2c4.com/ctmg/about/) tool written by Jason Donenfeld (aka zx2c4) on my Linux machines. I wanted a tool like ctmg for OpenBSD. This tool requires that you have [doas](https://man.openbsd.org/doas) configured.

## doas configuration
```bash
# if you haven't already, add a line like this to /etc/doas.conf
permit persist BobBoblaw

# allow user bob to execute superuser commands
```

### Installation
Simply run:

```bash
doas make install

# to uninstall
doas make uninstall
```
There are no dependencies, but this tool only works for OpenBSD. 

### Usage
```bash
pecm new 1000 MB
pecm open container.ct # (pecm expects your container to have the .ct extension at runtime)
pecm list
pecm close container
```

### Differences from ctmg
- Does not create sparse containers
- Does not include the 'delete' flag (I'll let you type rm)
- Currently spaces in container names are not supported (working on refactoring)

### Limitations
With the generic kernel you can only have 4 virtual devices open at once.
You have to compile a custom kernel to change this value. See the man page for [vnd](https://man.openbsd.org/vnd.4) for more info.

### License / Disclaimer
This project is licensed under the 3-clause BSD license. (See LICENSE.md)

I take no responsibility for you blowing stuff up.

Artwork courtesy of freepik (CC 3.0 BY)

