#!/bin/ksh
#
# pecm - puffy's encrypted container manager
# author: klosure
# https://gitlab.com/klosure/pecm 
# license: BSD 3-clause

VERSION="0.2"


# you must run this script as root as it mounts devices,
# it will not automatically invoke doas for you.
#if [ $(id -u) -ne 0 ] ; then
  #echo "Please run pecm as root"
  #exit 1
#fi


# -- GLOBAL VARS --
# the filename of our container (including path)
FILENAME="container.ct"
# the size of the container in megabytes
SIZE=1
# virtual node device number n
VNDN="vndn"
# virtual node device number n , partition a
VNDNA="vndna"
# sata device number n
SDN="sd"
# sata device number n ; c indicates the whole disk
SDNC="sdnc"
# the final mounting location of our unlocked container
UNLOCKED_MOUNTP="mnt"
# was there a name clash?
CLASH=1

# -- NEW --
# initialize the 'container'
fun_init_ct () {
    # validate args
    fun_check_args_new $@

    # check for name clashes
    fun_name_unique $FILENAME "NEW"
    
    # set global unlocked mountpoint
    fun_get_mountp
    
    # set file with zeroes
    dd if=/dev/zero of=$FILENAME bs=1M count=$SIZE 2> /dev/null

    # find an open node
    fun_set_slot "not"
    
    # associate it with the open vnd
    doas vnconfig $VNDN $FILENAME
    VNDNA=$VNDN"a"
    local RVNDNC="/dev/r"$VNDN"c"
    
    # partition container
    # (ugly here documents are ugly)
    doas fdisk -i $RVNDNC <<EOF
y

w
EOF
    
    # format container 
    doas disklabel -E $RVNDNC <<EOF
a
a


RAID

q

y

EOF
    echo "\n"
    # engage encryption splines
    fun_open_softraid "true"
}


# This function makes sure user provided name does not conflict with
# an existing mountpoint.
#
# If you are trying to create ~/container.ct and /mnt/container exists
# we want to fix the name. Containers need a unique name. Otherwise, when
# we go to unmount it is possible to we will try to unmount
# the incorrect device. This adds "-pecm" onto clashing names.
fun_name_unique () {
    local FN=$(echo "$1" | cut -d"." -f 1)
    for DEV in $(df) ; do
	# are we creating a new container...
	if [[ $DEV == *"$FN" && $2 == "NEW" ]] ; then
	    FILENAME=$FN"-pecm.ct"
	    CLASH=0
	fi
	# ... or opening a container?
	if [[ $DEV == *"$FN" && $2 == "OPEN" ]] ; then
	    CLASH=0
	    # mountpoint name clash detected
	fi
    done
}


fun_open_uniq () {
    for DEV in $(df) ; do
	echo $DEV
	echo $1                                                                    
        if [[ $DEV == *"$1" ]] ; then
            CLASH=0
            # mountpoint name clash detected                                                             
        fi
    done
}


fun_get_mountp () {
    # cut off .ct
    UNLOCKED_MOUNTP=$(echo "$FILENAME" | cut -d"." -f 1)
}


# -- OPEN --
# tell the OS that our 'container' is a virtual node (aka loop device)
fun_open_ct () {
    #check that filename has .ct extension
    FILENAME=$1
    local FN=$1

    # set unlocked mountpoint global
    fun_get_mountp
    
    # check number of args
    fun_check_open_close

    # check name clash
    fun_name_unique $FN "OPEN"
    if [[ $CLASH == 0 ]] ; then
	echo "Error - name clash detected with mountpoint:"
	echo "Is there a drive mounted with the same name as your container?"
	exit 1
    fi
    
    # find an open node
    fun_set_slot "not"
    
    # associate our virtual node with our file
    doas vnconfig $VNDN $FILENAME

    # vnd[n] w/ partition a 
    VNDNA=$VNDN"a"
    #echo "VNDNA = $VNDNA"

    fun_open_softraid "false"
}


fun_open_softraid () {
    local IS_INIT=$1
    # engage softraid and prepare for liftoff
    local MOUNT_MSG=$(doas bioctl -v -c C -l /dev/"$VNDNA" softraid0)
    SDN=$(echo $MOUNT_MSG | tail -c 4)
    # then grab the number of what /dev/sd* was used

    # shortcut to say "use the whole disk"
    SDNC=$SDN"c"

    # if we created a new container, write an fs
    if [[ $IS_INIT == "true" ]] ; then
	doas newfs /dev/r$SDNC
    fi

    # create the mountpoint for the unlocked container
    echo "[✓] Creating directory for mountpoint ..."
    mkdir $UNLOCKED_MOUNTP

    # mount our unlocked container
    doas mount /dev/$SDNC $UNLOCKED_MOUNTP
    echo "[✓] Mounted container at: ./$UNLOCKED_MOUNTP ..."
}

# refactor to allow spaces
# -- CLOSE --
# unmount the unlocked container
fun_close_ct () {
    fun_check_filename "$1"
    fun_get_mountp
    
    # check number of args
    fun_check_open_close

    local FOUND_CT=0
    for DEV in $(df) ; do
	if [[ "$DEV" == *"$UNLOCKED_MOUNTP" ]] ; then
	    FOUND_CT=1
	    break
	else
	    if [[ "$DEV" == "/dev/sd"* ]] ; then
		SDNC="$(echo "$DEV" | cut -d'/' -f 3)"
	    fi
	fi
    done

    if [[ "$FOUND_CT" == 1 ]] ; then	
        # unmount our unlocked container
	# VV doas
        if doas umount "$UNLOCKED_MOUNTP" ; then
	    echo "[✓] $UNLOCKED_MOUNTP was unmounted ..."
	fi

        # disengage softraid and prepare brakes for landing
	# VV doas
        if doas bioctl -d /dev/"$SDNC" ; then
	    echo "[✓] Softraid at /dev/$SDNC was disconnected ..."
	fi

	# find our vndn slot
	if fun_set_slot "$FILENAME" ; then
            # 'unconfigure' our virtual node (aka loop device)
            if doas vnconfig -u "$VNDN" ; then
		echo "[✓] Unconfiguring $VNDN ..."
		# delete the mountpoint folder we created during open
	        doas rm -r "$UNLOCKED_MOUNTP"
		echo "[✓] Mountpoint directory $UNLOCKED_MOUNTP removed ..."
	    else
		echo "[✗] Error unconfiguring vnd ..."
		exit 1
	    fi
	else
	    echo "[✗] Error matching vnd to mountpoint ..."
	    exit 1
	fi
      
    else
	echo "[✗] The open container was not found ..."
	exit 1
    fi
}

# refactor to allow spaces
# find what vndn slot we want
fun_match_slot () {
    local TARGET_VAL="$1"
    local CURR_SLOT="vnd0"
    # VV doas call + fix for spaces VV
    for NODE in $(doas vnconfig -l) ; do
	# check if this array item contains our target value
        if [[ "$NODE" == "$TARGET_VAL" ]] ; then
	    VNDN="$CURR_SLOT"
	    return 0
	elif [[ "$NODE" == "vnd1:" ]] ; then
	    CURR_SLOT="vnd1"
	elif [[ "$NODE" == "vnd2:" ]] ; then
	    CURR_SLOT="vnd2"
	elif [[ "$NODE" == "vnd3:" ]] ; then
	    CURR_SLOT="vnd3"
        fi
    done
    return 1
}


# set the global slot variable, (sok)
fun_set_slot () {
    if fun_match_slot "$1" ; then
	if [[ "$1" == "not" ]] ; then
	    echo "[✓] $FILENAME is being associated with $VNDN ..."
	fi
    else
	# no vnd slots were available..
	echo "[✗] no virtual node slots are currently open.."
	exit 1
    fi
}

# (sok)
# -- LIST --
# show the user containers that are currently open
fun_list_ct () {
    # vv fix for spaces vv
    for NODE in $(doas vnconfig -l) ; do
	# check if this array item contains "*.ct"
        if [[ "$NODE" == *".ct" ]] ; then
	    echo "$NODE"
        fi
    done
}


# checks the number of arguments passed in (sok)
fun_check_open_close () {
    if [[ $# > 3 ]] ; then
       echo "[✗] too many arguments passed in"
       exit 1
    fi
}


# used to check arguments for errors (sok)
fun_check_args_new () {
    if [[ $# > 5 ]] ; then
       echo "[✗] too many arguments passed in"
       exit 1
    fi

    if [[ "$2" -lt 1 || "$2" -gt 900000 ]] ; then
	fun_error_init
    fi

    if [[ "$3" == "MB" || "$3" == "mb" || "$3" == "Mb" ]] ; then
	SIZE="$2"
    elif [[ "$3" == "GB" || "$3" == "gb" || "$3" == "Gb" ]] ; then
	fun_convert_to_mb "$2"
    else
	fun_error_init
    fi
    fun_check_filename "$1"
}


# checks if a given filename contains our ".ct" extension (sok)
fun_check_filename () {
    FILENAME="$1"
    EXT="$(echo "$FILENAME" | cut -d'.' -f 2)"
    if [[ "$EXT" != "ct" ]] ; then	
	FILENAME="$FILENAME.ct"
    fi
}


# converts from GB to MB (sok)
fun_convert_to_mb () {
    local MB="$(($1*1000))"
    SIZE="$MB"
}


# run when ctrl-c is pressed (sok)
fun_cleanup () {
    echo "caught sigint, cleaning up..."
    vnconfig -u "$VNDN"
    rm "$FILENAME"
    exit 0
}


# print help/version info
fun_print_info () {
    echo "pecm - puffy's encrypted container manager"
    fun_print_ver
    echo "license: bsd 3-clause"
    echo "author: klosure"
    echo "site: https://gitlab.com/klosure/pecm"
    fun_print_usage
}


# print usage info
fun_print_usage () {
    echo "usage: pecm new myfilename 1000 MB"
    echo "       pecm open myfilename"
    echo "       pecm close myfilename"
    echo "       pecm list"
    echo "       pecm help"
    echo "       pecm version"
}


# print version info
fun_print_ver () {
    echo "version: $VERSION"
}


# print error message
fun_error () {
    echo "[✗] Please use one of the following flags:"
    fun_print_usage
    exit 1
}


# error during init of new container
fun_error_init () {
    echo "[✗] You entered an invalid size. Please enter a value withing 0 < x < 900000"
    echo "Units should be in MB or GB"
    exit 1
}


# -- ENTRY POINT--
# parse what arguments were passed in
# call a corresponding function (sok)
trap 'fun_cleanup' INT # catch a ctrl-c
case "$1" in
    help) shift;	 fun_print_info "$@" ;;
    version) shift;      fun_print_ver "$@" ;;
    new) shift;	         fun_init_ct "$@" ;;
    close) shift;	 fun_close_ct "$@" ;;
    list) shift;	 fun_list_ct "$@" ;;
    open) shift;	 fun_open_ct "$@" ;;
    *)		         fun_error "$@" ;;
esac
exit 0
